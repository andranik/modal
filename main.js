var modal = document.querySelector('.modal');
var overlay = document.querySelector('.overlay');
var main = document.querySelector('.main');


main.onclick = function() {
    modal.className += " is-active";
    overlay.className += " is-active";
    setTimeout(function(){modal.className = 'modal'},3000);
    setTimeout(function(){overlay.className = 'overlay'},3000);
};